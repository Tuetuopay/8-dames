//
//  OSCompatibility.h
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

/* This file is intended to bypass differences between differents OSes,
 * with system definitions and other stuff.
 *
 * Every #include of members of the C++ STL should be added in this file.
 */

#ifndef _OS_COMPATIBILITY_H
#define _OS_COMPATIBILITY_H

/* # # # # # # # # # # COMMON CODE # # # # # # # # # # */
/* STL includes */
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <limits>
#include <errno.h>
#include <map>
#include <pthread.h>

#ifndef _USE_MATH_DEFINES	/* M_PI & co consts */
# define _USE_MATH_DEFINES
#endif
#include <cmath>

std::string stripFolderSeparators (std::string path);
std::string shortenPath (std::string path);
std::string getDirName (std::string path);
void mkdir (std::string path);

long clamp (long value, long min, long max);

#if UINT_MAX >> 32 == 0
 typedef unsigned int uint32;
#elif ULONG_MAX >> 32 == 0
 typedef unsigned long uint32;
#else
 typedef unsigned long int uint32;
#endif
#if INT_MAX >> 32 == 0
 typedef int int32;
#elif LONG_MAX >> 32 == 0
 typedef long int32;
#else
 typedef long int int32;
#endif

#if (ULONG_MAX >> 32) >> 32 == 0		/* two >> operators, else will cause an overflow */
 typedef unsigned long uint64;
#elif (ULONG_LONG_MAX >> 32) >> 32 == 0
 typedef unsigned long long uint64;
#else
 typedef unsigned long long int uint64;	/* BTW, don't know if "unsigned long long int" exists xD */
#endif

#if (LONG_MAX >> 32) >> 32 == 0
 typedef long int64;
#elif (LLONG_MAX >> 32) >> 32 == 0
 typedef long long int64;
#else
 typedef long long int int64;
#endif

#define UINT32_LAST_BIT	(1 << 31)

/* # # # # # # # # # # OS SPECIFIC CODE # # # # # # # # # # */
/* Windows specific code */

#ifdef __linux__
# ifndef __LINUX__
#  define __LINUX__
# endif
#endif

#ifdef _WIN32
# ifndef __WIN32__
#  define __WIN32__
# endif
#endif

#ifdef __APPLE__
# ifndef __OSX__
#  define __OSX__
# endif
#endif

/* # # # # # # # # # # *more* COMMON CODE # # # # # # # # # # */
/* Math defines */
#ifndef M_E
#define M_E         2.71828182845904523536028747135266250   /* e              */
#endif
#ifndef M_LOG2E
#define M_LOG2E     1.44269504088896340735992468100189214   /* log2(e)        */
#endif
#ifndef M_LOG10E
#define M_LOG10E    0.434294481903251827651128918916605082  /* log10(e)       */
#endif
#ifndef M_LN2
#define M_LN2       0.693147180559945309417232121458176568  /* loge(2)        */
#endif
#ifndef M_LN10
#define M_LN10      2.30258509299404568401799145468436421   /* loge(10)       */
#endif
#ifndef M_PI
#define M_PI        3.14159265358979323846264338327950288   /* pi             */
#endif
#ifndef M_PI_2
#define M_PI_2      1.57079632679489661923132169163975144   /* pi/2           */
#endif
#ifndef M_PI_4
#define M_PI_4      0.785398163397448309615660845819875721  /* pi/4           */
#endif
#ifndef M_1_PI
#define M_1_PI      0.318309886183790671537767526745028724  /* 1/pi           */
#endif
#ifndef M_2_PI
#define M_2_PI      0.636619772367581343075535053490057448  /* 2/pi           */
#endif
#ifndef M_2_SQRTPI
#define M_2_SQRTPI  1.12837916709551257389615890312154517   /* 2/sqrt(pi)     */
#endif
#ifndef M_SQRT2
#define M_SQRT2     1.41421356237309504880168872420969808   /* sqrt(2)        */
#endif
#ifndef M_SQRT1_2
#define M_SQRT1_2   0.707106781186547524400844362104849039  /* 1/sqrt(2)      */
#endif

/* Used for multi-platforming */
#ifdef __WIN32__
# define FOLDER_SEPARATOR	'\\'
# define FOLDER_SEPARATOR_OPPOSITE	'/'
#else
# define FOLDER_SEPARATOR	'/'
# define FOLDER_SEPARATOR_OPPOSITE	'\\'
#endif

#endif /* defined(_OS_COMPATIBILITY_H) */
