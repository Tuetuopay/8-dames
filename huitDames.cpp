#include <iostream>
#include <vector>
#include <time.h>
#include <fstream>

#include "Board.h"
#include "SystemProfiler.h"

void placeNext (Board& board, int lastColumn, int depth);
int removeDoubles (std::vector<std::vector<int>>& boards);
bool checkForQueen (std::vector<std::vector<int>>& boards, int x, int y);
void printToFile (std::ostream& out, std::vector<std::vector<int>>& boards, int s);

std::vector<std::vector<int>>	solutions (0);
int maxDepth = 0;
int boardSize = 0;

int main ()	{
	int s = 0;
	
	do {
		std::cout << "Entrez une taille d'échiquier [1,99] ";
		std::cin >> s;
	} while (s <= 0 || s > 99);
	
	/* Size s board */
	Board board (s);
	boardSize = s;
	long beginTime = 0;
	
	int choix = 0;
	do {
		std::cout << "\t1. Placer la première dame\n\t2. Toutes les dames posibles de l'échiquier \n > ";
		std::cin >> choix;
	} while (choix != 1 && choix != 2);
	
	if (choix == 1)	{
		int x, y;
		do	{
			std::cout << "Position x de la première dame ? ";
			std::cin >> x;
		} while (x < 0 || x >= s);
		do	{
			std::cout << "Position y de la première dame ? ";
			std::cin >> y;
		} while (y < 0 || y >= s);
		
		beginTime = time (NULL);
		
		board.placeQueenAt (x, y);
		
		placeNext (board, x, 1);
	}
	else if (choix == 2)
	{
		long oldCount = 0, oldTime = 0;
		std::cout << "I hope you have a lot of memory ...\n";
		beginTime = time (NULL);
		for (int x = 0; x < s; x++)
			for (int y = 0; y < s; y++)	{
				if (checkForQueen (solutions, x, y))
					continue;
				
				oldCount = solutions.size ();
				oldTime = time (NULL);
				std::cout << "Dame (" << x << ", " << y << ")\t";
				board.push ();
				board.placeQueenAt (x, y);
				placeNext (board, x, 1);
				board.pop ();
				std::cout << solutions.size () - oldCount << " nouvelles solutions\ten " << time (NULL) - oldTime << "s\t" << SystemProfiler::currentRAMUsage () / 1000 / 1000 << "Mo Utilisés\n";
			}
		std::cout << "\n";
	}
	
	beginTime = time (NULL) - beginTime;
	
	std::cout << "\n = = = = = " << solutions.size () << " solutions trouvées en " << beginTime << " s. = = = = = \n";
	// if (choix == 2)	std::cout << "(" << removeDoubles (solutions) << " doubles retirés.)\n";

	std::string c = "";
	do	{
		std::cout << "Afficher les " << solutions.size () << " solutions ? (y/n/f) ";
		std::cin >> c;
	} while (c != "y" && c != "Y" && c != "n" && c != "N" && c != "f" && c != "F");

	if (c == "n" || c == "N")	return 0;
	if (c == "f" || c == "F")	{
		std::cout << "Nom du fichier ";
		std::cin >> c;
		std::ofstream f (c.c_str ());
		printToFile (f, solutions, s);
		return 0;
	}
	
	printToFile (std::cout, solutions, s);
	
	std::cout << " = = = = = " << solutions.size () << " solutions trouvées en " << beginTime << " s. = = = = = \n";
	
	return 0;
}

void placeNext (Board& board, int lastColumn, int depth)	{
	int col = (lastColumn >= board.size () - 1) ? 0 : lastColumn + 1;
	if (depth > board.size ())	return;
	if (depth == board.size ())	solutions.push_back (board.status ());
	
	if (depth > maxDepth)	maxDepth = depth;
	
	for (int i = 0; i < board.size (); i++)	{
		if (board.canPlaceQueenAt (col, i))	{
			board.push ();
			board.placeQueenAt (col, i);
			if (depth != board.size () || board.countQueens () != board.size ())	/* Here we found a solution */
				placeNext (board, col, depth + 1);
			board.pop ();
		}
	}
}

bool isBoardInside (const std::vector<std::vector<int>>& boards, std::vector<int>& bd)	{
	bool boardOK = true;
	for (int i = 0; i < boards.size (); i++)	{
		boardOK = true;
		for (int j = 0; j < boardSize; j++)
			if (boards[i][j] != bd[j])
				boardOK = false;
		if (boardOK)	return true;
	}
	return false;
}
int removeDoubles (std::vector<std::vector<int>>& boards)	{
	int doubles = 0;
	std::vector<std::vector<int>> vec = boards;
	
	boards.resize (0);
	for (int i = 0; i < vec.size (); i++)	{
		if (!isBoardInside (boards, vec[i]))
			boards.push_back (vec[i]);
		else
			doubles++;
	}
	
	return doubles;
}
bool checkForQueen (std::vector<std::vector<int>>& boards, int x, int y)	{
	for (int i = 0; i < boards.size (); i++)
		if (boards[i][x] == y)
			return true;
	return false;
}

void printToFile (std::ostream& out, std::vector<std::vector<int>>& boards, int s)	{
	std::vector<std::string> cur, total;
	cur.resize (s + 2);
	total.resize (s + 2);
	Board bd (boardSize);
	
	for (int i = 0; i < boards.size (); i++)	{
		cur = bd.loadStatus (boards[i]).textBoard ();
		for (int j = 0; j < s + 2; j++)
			total[j] += cur[j] + " ";
		
		if ((i + 1) % (70 / s) == 0)	{
			for (int i = 0; i < s + 2; i++)
				out << total[i] + "\n";
			total.resize (0);
			total.resize (s + 2);
		}
	}
	
	for (int i = 0; i < s + 2; i++)
		out << total[i] + "\n";
}

/*
 std::vector<std::string> cur, total;
 cur.resize (s + 2);
 total.resize (s + 2);
 
 for (int i = 0; i < solutions.size (); i++)	{
 cur = solutions[i].textBoard ();
 for (int j = 0; j < s + 2; j++)
 total[j] += cur[j] + " ";
 
 if ((i + 1) % (70 / s) == 0)	{
 for (int i = 0; i < s + 2; i++)
 std::cout << total[i] + "\n";
 total.resize (0);
 total.resize (s+2);
 }
 }
 
 for (int i = 0; i < s + 2; i++)
 std::cout << total[i] + "\n";
 */












