//
//  SystemProfiler.cpp
//  PokeFPS
//
//  Created by Alexis on 29/09/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "SystemProfiler.h"

#ifdef __APPLE__
# include <mach/mach.h>
#elif defined(__WIN32__)
# include <windows.h>
# include <psapi.h>
# include <pdh.h>
#elif defined(__LINUX__)	/* Linux only does no need fancy stupeh headers :3 */
# include <cstdio>
# include <cstdlib>
# include <cstring>
#endif

unsigned long parseLine(char* line, ){
	unsigned long i = strlen(line);
	while (*line < '0' || *line > '9') line++;
	line[i-3] = '\0';
	i = atoi(line);
	return i;
}

SystemProfiler::SystemProfiler ()
{
	
}

SystemProfiler::~SystemProfiler ()
{
	
}

unsigned long SystemProfiler::currentRAMUsage()
{
	unsigned long usedRAM = 0;
#if defined(__APPLE__)
	struct task_basic_info t_info;
	mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;
	
	if (KERN_SUCCESS != task_info(mach_task_self(),
								  TASK_BASIC_INFO, (task_info_t)&t_info,
								  &t_info_count))
		usedRAM = -1;
	else
		usedRAM = t_info.resident_size;
	// resident size is in t_info.resident_size;
	// virtual size is in t_info.virtual_size;
#elif defined(__LINUX__)
	FILE* file = fopen("/proc/self/status", "r");
	unsigned long result = -1;
	char line[128];
    
	
	while (fgets(line, 128, file) != NULL){
		if (strncmp(line, "VmSize:", 7) == 0){
			result = parseLine(line);
			break;
		}
	}
	fclose(file);
	usedRAM = result * 1024;	/* Linux is giving it's values in KB ! */
#elif defined(__WIN32__)
	// PROCESS_MEMORY_COUNTERS_EX pmc;
	PROCESS_MEMORY_COUNTERS pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
	// usedRAM = (unsigned long)pmc.PrivateUsage;
	usedRAM = (unsigned long)pmc.WorkingSetSize;
#else
	/* Meh ... are we running Arduino os ? :p */
#endif
	
	return usedRAM;
}

std::string SystemProfiler::ramToString (unsigned long ram)	{
	ram /= 1024;
	std::ostringstream oss;
	oss << ram;
	std::string str = oss.str(), buf = "";
	int count = 0;
	for (long i = str.length() - 1; i >= 0; i--) {
		if (count == 3)	{
			buf += ',';
			count = 0;
		}
		buf += str.at (i);
		count ++;
	}
	str = "";
	for (long i = buf.length() - 1; i >= 0; i--)
		str += buf.at (i);
	str += " KB";
	return str;
}

void SystemProfiler::printCurrentRamUsage (std::string prefix)	{
	std::cout << prefix << "Ram: used = " << ramToString (currentRAMUsage()) << std::endl;
}









