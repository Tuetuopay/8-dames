//
//  OSCompatibility.cpp
//
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#include "OSCompatibility.h"
#include "StringUtils.h"

std::string shortenPath (std::string path)	{
	/* Will transform a path like "folder/subfolder1/../subfolder2/"
	 *						 into "folder/subfolder2/"
	 */
	std::vector<std::string> folders = split (path, FOLDER_SEPARATOR);
	std::vector<std::string> final;
	std::string str = "";

	for (long i = folders.size () - 1; i >= 0; i--)	{	/* We are running backwards because of the "previous folder" => ".." */
		if (folders[i] == ".")	/* Same folder, skip */
			;
		else if (folders[i] == "..")	/* Previous folder, jumping over next one */
			i--;
		else
			final.push_back (folders[i]);
	}

	for (long i = final.size () - 1; i >= 0; i--)	/* final has been built backwards */
		str += final[i] + FOLDER_SEPARATOR;
	str = stripFolderSeparators (str);
	return str;
}


std::string stripFolderSeparators (std::string path)	{
	std::string str = "";
	/* Removing possible multiple '\' */
	if (path == "")	return path;
	str += path[0];
	for (int i = 1; i < path.length (); i++)	{
		if (path[i] == FOLDER_SEPARATOR && path[i-1] != FOLDER_SEPARATOR)
			str += FOLDER_SEPARATOR;
		else if (path[i] != FOLDER_SEPARATOR)
			str += path[i];
	}
	if (path[path.size () - 1] == FOLDER_SEPARATOR)	{
		long i = path.size () - 1;
		while (path[--i] == FOLDER_SEPARATOR && i >= 0);
		path.resize (i);
	}
	return str;
}

std::string getDirName (std::string path)	{
	std::string str = "";
	
	for (int i = 0; i < path.length (); i++)	{
		if (path[i] == FOLDER_SEPARATOR_OPPOSITE)
			str += FOLDER_SEPARATOR;
		else
			str += path[i];
	}
	str = stripFolderSeparators (str);
	str = shortenPath (str);
	
	if (str.length() >= 1)		/* str.back () would crash on some OSes (Win32, not to mention) if str is empty */
		if (str.back() == FOLDER_SEPARATOR)
			str.pop_back();

	return str;
}

void mkdir (std::string path)	{
	std::string mkdircmd = "mkdir ";
	
#ifndef __WIN32__
	/* mkdir on unix-based systems needs the -p options to create all subdirectories 
	 * ex: chunks/earth/france/paris/
	 */
	mkdircmd += "-p ";
#endif
	
	path = getDirName(path);	/* Convert to os-specific path format */
	system ((mkdircmd + getDirName (path)).c_str()); /* Creating the directory */
}

long clamp (long value, long min, long max)	{
	if (value > max)	value = max;
	if (value < min)	value = min;
	return value;
}



