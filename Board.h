
#ifndef _BOARD_H
#define _BOARD_H

#include <vector>
#include <stack>
#include <string>

template<typename T>
T min (T a, T b);
template<typename T>
T max (T a, T b);

typedef struct	{
	bool hasQueen;
	bool queenBlocks;
} Tile;

class Board	{
public:
	Board (unsigned int size = 8);
	
	bool canPlaceQueenAt (int x, int y);
	bool placeQueenAt (int x, int y);
	bool hasQueenAt (int x, int y)	{ return _board[x][y].hasQueen; }
	
	int countQueens () const;
	
	inline unsigned int size () const { return _size; }
	
	inline void push ()	{ _stack.push (_board); }
	inline void pop ()		{ _board = _stack.top ();	_stack.pop (); }
	
	void print () const;
	std::vector<std::string> textBoard () const;
	
	std::vector<int> status () const;
	Board& loadStatus (std::vector<int> st);
	
	void clear ();
	
	bool operator== (const Board& bd) const;
	bool operator!= (const Board& bd) const;
	
private:
	std::vector<std::vector<Tile> >	_board;
	int _size;
	
	std::stack<std::vector<std::vector<Tile> > >	_stack;
};

#endif
