//
//  SystemProfiler.h
//  PokeFPS
//
//  Created by Alexis on 29/09/13.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#ifndef _SYSTEM_PROFILER_H
#define _SYSTEM_PROFILER_H

#include "OSCompatibility.h"

class SystemProfiler
{
public:
	SystemProfiler ();
	~SystemProfiler ();
	
	static unsigned long currentRAMUsage ();
	static std::string ramToString (unsigned long ram);
	
	static void printCurrentRamUsage (std::string prefix = "");
};

#endif /* defined(_SYSTEM_PROFILER_H) */
