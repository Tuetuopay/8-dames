
#include "Board.h"

#include <iostream>
#include <sstream>

template<typename T>
T min (T a, T b)	{
	return (a < b) ? a : b;
}
template<typename T>
T max (T a, T b)	{
	return (a < b) ? b : a;
}

template<typename T>
bool isInside (T a, T min, T max)	{
	return (a >= min && a < max);
}

Board::Board (unsigned int size)	{
	Tile empty = {false, false};
	
	_board.resize (size);
	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)	{
			_board.at (i).resize (size);
			_board[i][j] = empty;
		}
	
	_size = size;
}

bool Board::canPlaceQueenAt (int x, int y)	{
	return !_board[x][y].queenBlocks;
}

bool Board::placeQueenAt (int x, int y)	{
	if (!isInside (x, 0, _size) || !isInside (y, 0, _size))	return false;
	if (!canPlaceQueenAt (x, y) || _board[x][y].hasQueen)	return false;
	
	_board[x][y].hasQueen = true;
	
	int nY = 0;
	for (int i = 0; i < _size; i++)	{
		/* Bottom left to top right */
		nY = i + y - x;
		if (isInside (nY, 0, _size))
			_board[i][nY].queenBlocks = true;
		
		/* Top left to bottom right */
		nY = -i + y + x;
		if (isInside (nY, 0, _size))
			_board[i][nY].queenBlocks = true;
		
		_board[i][y].queenBlocks = true;
		_board[x][i].queenBlocks = true;
	}
	
	_board[x][y].queenBlocks = false;
	
	return true;
}

/*
 * x + b = y
 * b = y - x
 */

int Board::countQueens ()	const	{
	int count = 0;
	for (int i = 0; i < _size; i++)
		for (int j = 0; j < _size; j++)
			if (_board[i][j].hasQueen)
				count++;
	return count;
}

void Board::print () const	{
	std::vector<std::string> vec = textBoard ();
	
	for (int i = 0; i < _size + 2; i++)
		std::cout << vec[i] << "\n";
}

std::vector<std::string> Board::textBoard () const	{
	std::vector<std::string> ret (0);
	ret.resize (_size + 2);
	
	ret[0] += "+";
	for (int i = 0; i < _size * 2 + 1; i++)
		ret[0] += "-";
	ret[0] += "+";
	for (int i = 0; i < _size; i++)	{
		ret[i + 1] += "| ";
		for (int j = 0; j < _size; j++)	{
			if (_board[i][j].hasQueen && _board[i][j].queenBlocks)
				ret[i + 1] += "@ ";
			else if (_board[i][j].hasQueen)
				ret[i + 1] += "# ";
			else if (_board[i][j].queenBlocks)
				ret[i + 1] += ". ";
			else
				ret[i + 1] += "  ";
		}
		ret[i + 1] += "|";
	}
	ret[_size + 1] += "+";
	for (int i = 0; i < _size * 2 + 1; i++)
		ret[_size + 1] += "-";
	ret[_size + 1] += "+";
	
	return ret;
}

bool Board::operator== (const Board& bd) const	{
	if (_size != bd.size ()) return false;
	for (int i = 0; i < _size; i++)
		for (int j = 0; j < _size; j++)
			if (_board[i][j].hasQueen != bd._board[i][j].hasQueen)
				return false;
	
	return true;
}
bool Board::operator!= (const Board& bd) const	{
	return !((*this) == bd);
}

std::vector<int> Board::status () const	{
	std::vector<int> ret;
	ret.resize (_size);
	
	for (int i = 0; i < _size; i++)
		for (int j = 0; j < _size; j++)
			if (_board[i][j].hasQueen)	{
				ret[i] = j;
			}
	
	return ret;
}
Board& Board::loadStatus (std::vector<int> st)	{
	clear ();
	for (int i = 0; i < st.size (); i++)
		placeQueenAt (i, st[i]);
	return *this;
}

void Board::clear ()	{
	for (int i = 0; i < _size; i++)
		for (int j = 0; j < _size; j++)
			_board[i][j].hasQueen = _board[i][j].queenBlocks = false;
}





